#!/bin/sh
export HEKETI_CLI_SERVER=http://127.0.0.1:7070
CLUSTER_COUNT=`heketi-cli cluster list | wc -l`
if [ CLUSTER_COUNT -eq 1 ]
then
  heketi-cli cluster create
fi
CLUSTER_ID=`heketi-cli cluster list | tail -n 1 | grep -o -E  "Id:.*?\s" | sed "s/Id://"`
CLUSTER="--cluster=$CLUSTER_ID"
HOST="--management-host-name=$1 --storage-host-name=$1"
ZONE="--zone=1"
NODE_ID=`heketi-cli node add $CLUSTER $HOST $ZONE | grep -v -i "Cluster" | grep -i "id" | cut -d : -f 2 | xargs`
heketi-cli device add --name=$2 --node=$NODE_ID
